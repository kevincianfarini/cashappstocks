# Cash App Application Project

## Run the Project

This project will be easiest to run through Android studio. It is a standard Android gradle build project. Import it, allow the IDE to index, and running on a virtual device or physical device should suffice.

If for some reason you prefer not to use the IDE to install this application you can use the gradle wrapper tasks to do so from the command line.

You may navigate to the root of this project and invoke the `./gradlew installDebug` command to automate this process for you. For more information on how to run this from the command line, please see [the Android documentation](https://developer.android.com/studio/build/building-cmdline#DebugMode).

## Trade Offs Made

### Abondoning `viewModelScope`

`viewModelScope` is a popular extension property used in a lot of MVVM architected projects to launch Kotlin coroutines within a ViewModel. I decided not to use this scope for my concurrency mainly for testing reasons.

The benefits of `viewModelScope` granted to a user are automatic cancellation of coroutines within a ViewModel when `ViewModel.onCleared` is called and ease of access. However, it does present an interesting problem for testing. How can we ensure that all running corotuines have finished during tests?

We need a parent scope.

Therefore, I have decided to manually implement `CoroutineScope` on my ViewModels via interface delegation. While there is a small amount of dependency injection setup involed, it provides more flexibility for us during testing.

```kotlin
class MyViewModel @Inject (
    @Named("viewModelScope") scope: CoroutineScope
) : ViewModel(), CoroutineScope by scope {

    override fun onCleared() {
        super.onCleared()
        cancel()
    }
}
```

Architecting our ViewModels like this allows us to inject a scope at runtime with dependency injection, while also allowing us more granular control while testing.

```kotlin
@Test fun testFoo() = runBlocking { // this: CoroutineScope
    val viewModel = MyViewModel(this)

    viewModel.launchSomeAsyncTask()

    assertTrue(viewModel.taskWasLaunched)
}
```

If the above exmaple is contrived, I recommend looking at `PortfolioViewModel.kt` as well as `PortfolioModule.kt`.

This _does_ require that we make sure to clean up corotines when the view model is cleared. If this project were to scale, I would introduce a base view model class to account for this.

### Avoiding merge tags in customs views used within lists

Normally I would use `<merge />` tags to create a custom compound view. However, during this project I realized that `tools:listitems` has a bug rendering list items in the preview that are merge tags. I've actually filed a bug to the Android Studio issue tracker because this issue took me longer than I care to admite to figure out.

You can find that bug [here](https://issuetracker.google.com/issues/171235563).

### Using Hilt over Vanilla Dagger

Hilt is a recently released Dependency Injection library built on top of Dagger that eliminates a lot of boilerplate code that Dagger requires. I opted to use this over just Dagger because I wanted to handle process death with `SavedStateHandle`. I recently
had to manually implement assisted injection for a view model and I found it to be more effort than I was expecting.

However, when I included the hilt library, I started to get a DEX count issue during the build. I was not able to figure out which dependencies were the culprit. Considering some of my projects at work have far, far more dependencies than I used here, I'm not quite sure what is hogging the DEX method counts.

I ended up enabling multidex. Given more time I would dig into this issue more, I was not happy with this solution.

### Dropping LiveData in favor of StateFlow

I have several problems with `LiveData`, the biggest being the difficulty for testing. LiveData requires a LifeCycle in order to be observed, which cannot be used within normal JUint tests. The view model coroutine scoping explained above paired with the Turbine library made testing my
view models extremely easy.

For example, testing a flow within my viewmodel was this easy.

```kotlin
@Test fun `network type change calls fetch stocks`() = runBlocking {
    val viewModel = PortfolioViewModel(SavedStateHandle(), repository, this)

    viewModel.networkStatusFlow.test {
        expectItem() // discard default
        Assert.assertEquals(NetworkPending, expectItem())
        Assert.assertEquals(NetworkSuccess, expectItem())
        expectNoEvents()
    }
}
```

As far as I'm concerned, the more code you can test with regular JUnit instead of instrumentation tests, the better.

I'm aware that `StateFlow` is an experimental API, however similar functionality could be achieved with a private channel and a regular instance of `Flow`.

## 3rd party libraries

I used a fair amount of dependencies that I've grown accustomed to. Had this project stopped here, I probably would have used less. I was told, however, that I would be iterating on this project during another portion of the interview. I opted to go with what I was comfortable with in order to reduce any amount of friction experienced during the interview.

I'm going to omit Kotlin dependencies and AndroidX dependencies from this list seeing as they're both either supported or promoted by Google.

I used Retrofit for simplifying network requests, Moshi for JSON deserialization. I opted to use Moshi codegen because I prefer compile time safety over runtime errors, even if it means longer compile times.

I also opted to use the great [material dialogs library](https://github.com/afollestad/material-dialogs) to simplify making bottom sheets which I used for a stock detail view. This is a convenience wrapper around the Andriod dialog APIs.

Finally, I opted to use [Turbine]() to test flows within my view models. This was my first time using this library, but I found to to be quite helpful. It made testing my view models much, much easier.