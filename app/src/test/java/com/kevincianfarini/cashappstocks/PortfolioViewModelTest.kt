package com.kevincianfarini.cashappstocks

import androidx.lifecycle.SavedStateHandle
import app.cash.turbine.test
import com.kevincianfarini.cashappstocks.data.models.*
import com.kevincianfarini.cashappstocks.data.network.NetworkType
import com.kevincianfarini.cashappstocks.data.network.repository.PortfolioRepository
import com.kevincianfarini.cashappstocks.features.portfolio.viewmodels.PortfolioViewModel
import kotlinx.coroutines.*
import org.junit.Assert
import org.junit.Test
import java.util.*
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class PortfolioViewModelTest {

    @Test fun `network type change calls fetch stocks`() = runBlocking {
        val repository = RepositoryTestDouble {}
        val viewModel = PortfolioViewModel(SavedStateHandle(), repository, this)
        viewModel.networkType = NetworkType.REGULAR

        viewModel.portfolioFlow.test {
            Assert.assertEquals(0, expectItem().size)
            expectNoEvents()
        }

        viewModel.networkStatusFlow.test {
            expectItem() // discard default
            Assert.assertEquals(NetworkPending, expectItem())
            Assert.assertEquals(NetworkSuccess, expectItem())
            expectNoEvents()
        }
    }

    @Test fun `network type error causes NetworkFailure emission`() = runBlocking {
        val repository = RepositoryTestDouble {}
        val viewModel = PortfolioViewModel(SavedStateHandle(), repository, this)
        viewModel.networkType = NetworkType.ERROR

        viewModel.networkStatusFlow.test {
            expectItem()
            Assert.assertEquals(NetworkPending, expectItem())
            Assert.assertTrue(expectItem() is NetworkFailure)
            expectNoEvents()
        }
    }

    @Test fun `network state is saved in view model SavedStateHandle`() = runBlocking {
        val state = SavedStateHandle()
        val viewModel = PortfolioViewModel(state, RepositoryTestDouble {}, this)
        viewModel.networkType = NetworkType.ERROR

        Assert.assertEquals(NetworkType.ERROR, state["key.network_type"])
    }

    @Test fun `portfolio state is saved in view model SavedStateHandle`() = runBlocking {
        val state = SavedStateHandle()
        val stock = Stock(
            "x",
            "y",
            Monetary(Currency.getInstance("USD"), 1),
            1,
            Date()
        )
        val viewModel = PortfolioViewModel(state, RepositoryTestDouble(listOf(stock)) {}, this)
        viewModel.networkType = NetworkType.REGULAR

        // consume all of the events
        viewModel.portfolioFlow.test {
            expectItem()
            expectItem()
        }

        Assert.assertEquals(stock, state.get<List<Stock>>("key.portfolio")?.first())
    }

    @Test fun `portfolio state is recreated on view model initialization`() = runBlocking {
        val state = SavedStateHandle()
        val stock = Stock(
            "x",
            "y",
            Monetary(Currency.getInstance("USD"), 1),
            1,
            Date()
        )
        state["key.portfolio"] = listOf(stock)
        val viewModel = PortfolioViewModel(state, RepositoryTestDouble(listOf(stock)) {}, this)

        viewModel.portfolioFlow.test {
            Assert.assertEquals(stock, expectItem().first())
            expectNoEvents()
        }
    }

    @Test fun `network type state is recreated on view model initialization`() = runBlocking {
        val state = SavedStateHandle()
        state["key.network_type"] = NetworkType.EMPTY
        val viewModel = PortfolioViewModel(state, RepositoryTestDouble {}, this)

        Assert.assertEquals(NetworkType.EMPTY, viewModel.networkType)
    }

    private class RepositoryTestDouble(
        private val mockStocks: List<Stock> = emptyList(),
        private val capture: (NetworkType) -> Unit
    ) : PortfolioRepository {
        override suspend fun getPortfolio(type: NetworkType): Portfolio {
            capture(type)

            if (type == NetworkType.ERROR) throw Exception()

            return Portfolio(mockStocks)
        }
    }
}