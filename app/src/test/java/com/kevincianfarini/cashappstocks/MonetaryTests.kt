package com.kevincianfarini.cashappstocks

import com.kevincianfarini.cashappstocks.data.models.Monetary
import com.kevincianfarini.cashappstocks.data.models.displayString
import com.kevincianfarini.cashappstocks.data.models.times
import org.junit.Assert
import org.junit.Test
import java.util.*

class MonetaryTests {

    @Test fun `Monetary multiplication produces correct result`() {
        val m = Monetary(Currency.getInstance("USD"), 1)
        Assert.assertEquals(m.copy(centsValue = m.centsValue * 3), m * 3)
    }

    @Test fun `Monetary produces correct display string`() {
        val m = Monetary(Currency.getInstance("USD"), 12345)
        Assert.assertEquals("$123.45", m.displayString)
    }
}