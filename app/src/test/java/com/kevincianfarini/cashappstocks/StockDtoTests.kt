package com.kevincianfarini.cashappstocks

import com.kevincianfarini.cashappstocks.data.models.Monetary
import com.kevincianfarini.cashappstocks.data.models.Stock
import com.kevincianfarini.cashappstocks.data.network.dtos.StockDto
import org.junit.Assert
import org.junit.Test
import java.util.*

class StockDtoTests {

    @Test fun `mapping stock dto to domain model produces correct results`() {
        val dto = StockDto(
            "x",
            "y",
            "USD",
            1,
            null,
            0
        )

        Assert.assertEquals(
            Stock(
                "x",
                "y",
                Monetary(Currency.getInstance("USD"), 1),
                0,
                Date(0)
            ),
            Stock.fromDto(dto)
        )
    }
}