package com.kevincianfarini.cashappstocks.common

/**
 * Configure objects as a way of passing data a single way. Configurable implementors
 * should use [configure] as the only way to ingest data.
 */
interface Configurable<in T : Any> {

    /**
     * Configre this object with data
     */
    fun configure(model: T)
}