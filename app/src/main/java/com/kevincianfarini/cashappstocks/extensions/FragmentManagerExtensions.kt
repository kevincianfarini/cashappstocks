package com.kevincianfarini.cashappstocks.extensions

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

inline fun FragmentManager.transaction(block: FragmentTransaction.() -> Unit) {
    beginTransaction().apply(block).commit()
}