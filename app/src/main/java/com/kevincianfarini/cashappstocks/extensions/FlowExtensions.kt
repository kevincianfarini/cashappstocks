package com.kevincianfarini.cashappstocks.extensions

import androidx.lifecycle.LifecycleCoroutineScope
import com.kevincianfarini.cashappstocks.data.models.*
import kotlinx.coroutines.flow.*
import java.lang.Exception

/**
 * Track the lifetime of [block] within this shared flow. [NetworkEvent] instances will be
 * emitted based on the progress of the request.
 */
suspend fun <T: Any> MutableStateFlow<NetworkEvent>.trackEvent(block: suspend () -> T): T? {
    emit(NetworkPending)

    val value = try {
        block()
    } catch (e: Exception) {
        emit(NetworkFailure(e))
        null
    }

    return value?.also { emit(NetworkSuccess) }
}

/**
 * A convenience method similar to [Flow.launchIn] specifically tailored to [LifecycleCoroutineScope]
 */
fun <T> Flow<T>.launchWhenResumedIn(scope: LifecycleCoroutineScope) = scope.launchWhenResumed {
    collect()
}