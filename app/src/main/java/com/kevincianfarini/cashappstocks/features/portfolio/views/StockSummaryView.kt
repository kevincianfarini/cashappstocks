package com.kevincianfarini.cashappstocks.features.portfolio.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.kevincianfarini.cashappstocks.R
import com.kevincianfarini.cashappstocks.common.Configurable
import com.kevincianfarini.cashappstocks.data.models.displayString
import com.kevincianfarini.cashappstocks.databinding.ViewStockSummaryBinding

class StockSummaryView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), Configurable<ListItemStockSummaryViewData> {

    private val binding: ViewStockSummaryBinding

    init {
        View.inflate(context, R.layout.view_stock_summary, this)
        binding = ViewStockSummaryBinding.bind(this)
    }

    override fun configure(model: ListItemStockSummaryViewData) = with(binding) {
        stockName.text = model.stock.name
        stockSymbol.text = model.stock.ticker
        stockPrice.text = model.stock.currentPrice.displayString
        root.setOnClickListener { model.onClickAction(model.stock) }
    }
}