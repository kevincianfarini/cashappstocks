package com.kevincianfarini.cashappstocks.features.portfolio.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.kevincianfarini.cashappstocks.R
import com.kevincianfarini.cashappstocks.common.Configurable
import com.kevincianfarini.cashappstocks.data.models.Stock
import com.kevincianfarini.cashappstocks.data.models.displayString
import com.kevincianfarini.cashappstocks.data.models.times
import com.kevincianfarini.cashappstocks.databinding.ViewStockDetailBinding
import java.text.SimpleDateFormat

class StockDetailView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), Configurable<Stock> {

    private val binding: ViewStockDetailBinding

    init {
        View.inflate(context, R.layout.view_stock_detail, this)
        binding = ViewStockDetailBinding.bind(this)
    }

    override fun configure(model: Stock) = with(binding) {
        stockName.text = model.name
        portfolioQuantity.text = model.quantity.toString()
        portfolioValue.text = (model.currentPrice * model.quantity).displayString
        stockPrice.text = model.currentPrice.displayString
        stockUpdatedDate.text = SimpleDateFormat.getDateInstance().format(model.lastUpdated)
    }
}