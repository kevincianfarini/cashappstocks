package com.kevincianfarini.cashappstocks.features.portfolio.fragments

import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.customview.customView
import com.google.android.material.snackbar.Snackbar
import com.kevincianfarini.cashappstocks.R
import com.kevincianfarini.cashappstocks.data.models.*
import com.kevincianfarini.cashappstocks.data.network.NetworkType
import com.kevincianfarini.cashappstocks.databinding.FragmentPortfolioBinding
import com.kevincianfarini.cashappstocks.extensions.launchWhenResumedIn
import com.kevincianfarini.cashappstocks.features.portfolio.adapters.PortfolioAdapter
import com.kevincianfarini.cashappstocks.features.portfolio.viewmodels.PortfolioViewModel
import com.kevincianfarini.cashappstocks.features.portfolio.views.StockDetailView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.onEach
import kotlin.properties.Delegates

@AndroidEntryPoint
class PortfolioFragment : Fragment() {

    private val viewModel: PortfolioViewModel by viewModels()
    private var _binding: FragmentPortfolioBinding? = null
    private val binding: FragmentPortfolioBinding get() = _binding!!
    private val portfolioAdapter = PortfolioAdapter(this::onStockClicked)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        subscribeToViewModel()
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentPortfolioBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        viewModel.networkType = viewModel.networkType ?: NetworkType.REGULAR
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.portfolio_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.network_regular -> {
            viewModel.networkType = NetworkType.REGULAR
            true
        }
        R.id.network_empty -> {
            viewModel.networkType = NetworkType.EMPTY
            true
        }
        R.id.network_error -> {
            viewModel.networkType = NetworkType.ERROR
            true
        }
        else -> false
    }

    private fun initViews() {
        initRecyclerView()
        initRefreshView()
    }

    private fun initRefreshView() = binding.swipeRefresh.setOnRefreshListener {
        viewModel.fetchStocks()
    }

    private fun initRecyclerView() = binding.portfolioList.apply {
        val manager = LinearLayoutManager(context)
        adapter = portfolioAdapter
        layoutManager = manager
        addItemDecoration(DividerItemDecoration(context, manager.orientation))
    }

    private fun subscribeToViewModel() = with(viewModel) {
        portfolioFlow.onEach(portfolioAdapter::configure).launchWhenResumedIn(lifecycleScope)
        networkStatusFlow.onEach(::onNetworkEventChange).launchWhenResumedIn(lifecycleScope)
    }

    private fun onNetworkEventChange(event: NetworkEvent) = when (event) {
        NetworkPending -> binding.swipeRefresh.isRefreshing = true
        NetworkSuccess -> binding.swipeRefresh.isRefreshing = false
        is NetworkFailure -> {
            handleError(event.error)
            binding.swipeRefresh.isRefreshing = false
        }
    }

    private fun handleError(e: Throwable) = Snackbar
        .make(binding.coordinatorLayout, e.localizedMessage ?: "", Snackbar.LENGTH_SHORT)
        .setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.colorError))
        .setActionTextColor(ContextCompat.getColor(requireContext(), android.R.color.white))
        .setAction(R.string.retry) { viewModel.fetchStocks() }
        .show()

    private fun onStockClicked(stock: Stock) = MaterialDialog(
        requireContext(),
        BottomSheet(LayoutMode.WRAP_CONTENT)
    ).show {
        customView(noVerticalPadding = true, view = StockDetailView(requireContext()).apply {
            configure(stock)
        })
    }

    companion object {
        const val TAG = "fragment.StockListFragment"
    }
}