package com.kevincianfarini.cashappstocks.features.portfolio.views

import com.kevincianfarini.cashappstocks.data.models.Stock

class ListItemStockSummaryViewData(
    val stock: Stock,
    val onClickAction: (Stock) -> Unit
)