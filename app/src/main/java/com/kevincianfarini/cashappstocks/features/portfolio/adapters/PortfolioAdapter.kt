package com.kevincianfarini.cashappstocks.features.portfolio.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kevincianfarini.cashappstocks.common.Configurable
import com.kevincianfarini.cashappstocks.data.models.Stock
import com.kevincianfarini.cashappstocks.features.portfolio.views.StockSummaryView
import com.kevincianfarini.cashappstocks.features.portfolio.views.ListItemStockSummaryViewData

class PortfolioAdapter(
    private val onItemClicked: (Stock) -> Unit
) : RecyclerView.Adapter<PortfolioAdapter.PortfolioItemViewHolder>(), Configurable<List<Stock>> {

    private lateinit var configuredModel: List<Stock>

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = PortfolioItemViewHolder(StockSummaryView(parent.context).apply {
        layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    })

    override fun onBindViewHolder(
        holder: PortfolioItemViewHolder,
        position: Int
    ) = holder.view.configure(
        ListItemStockSummaryViewData(configuredModel[position], onItemClicked)
    )

    override fun configure(model: List<Stock>) {
        configuredModel = model
        notifyDataSetChanged()
    }

    override fun getItemCount() = configuredModel.size

    class PortfolioItemViewHolder(val view: StockSummaryView) : RecyclerView.ViewHolder(view)
}