package com.kevincianfarini.cashappstocks.features.portfolio.viewmodels

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.kevincianfarini.cashappstocks.data.models.NetworkEvent
import com.kevincianfarini.cashappstocks.data.models.NetworkSuccess
import com.kevincianfarini.cashappstocks.data.models.Stock
import com.kevincianfarini.cashappstocks.data.network.NetworkType
import com.kevincianfarini.cashappstocks.data.network.repository.PortfolioRepository
import com.kevincianfarini.cashappstocks.extensions.trackEvent
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.onEach
import javax.inject.Named
import kotlin.properties.Delegates

class PortfolioViewModel @ViewModelInject constructor(
    @Assisted private val state: SavedStateHandle,
    private val repository: PortfolioRepository,
    @Named("viewModelScope") scope: CoroutineScope
) : ViewModel(), CoroutineScope by scope {

    private val _portfolioFlow = MutableStateFlow(state[KEY_PORTFOLIO] ?: emptyList<Stock>())
    private val _networkStatusFlow = MutableStateFlow<NetworkEvent>(NetworkSuccess)

    val networkStatusFlow = _networkStatusFlow.asStateFlow()
    val portfolioFlow = _portfolioFlow.asStateFlow().onEach { state[KEY_PORTFOLIO] = it }
    var networkType: NetworkType? by Delegates.observable(state[KEY_NETWORK_TYPE]) { _, old, new ->
        if (old != new) {
            fetchStocks()
            state[KEY_NETWORK_TYPE] = new
        }
    }

    override fun onCleared() {
        super.onCleared()
        cancel("$this onCleared")
    }

    fun fetchStocks() = launch {
        _networkStatusFlow.trackEvent {
            repository.getPortfolio(checkNotNull(networkType)).stocks
        }?.also { _portfolioFlow.value = it }
    }

    private companion object {
        const val KEY_PORTFOLIO = "key.portfolio"
        const val KEY_NETWORK_TYPE = "key.network_type"
    }
}