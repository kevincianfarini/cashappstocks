package com.kevincianfarini.cashappstocks

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kevincianfarini.cashappstocks.extensions.transaction
import com.kevincianfarini.cashappstocks.features.portfolio.fragments.PortfolioFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.transaction {
            replace(
                R.id.fragment_container,
                supportFragmentManager.findFragmentByTag(PortfolioFragment.TAG) ?: PortfolioFragment(),
                PortfolioFragment.TAG
            )
        }
    }
}