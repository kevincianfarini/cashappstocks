package com.kevincianfarini.cashappstocks.data.network.clients

import com.kevincianfarini.cashappstocks.data.network.dtos.PortfolioDto
import retrofit2.http.GET

interface PortfolioClient {

    @GET("portfolio.json")
    suspend fun getCurrentPortfolio(): PortfolioDto

    @GET("portfolio_empty.json")
    suspend fun getEmptyPortfolio(): PortfolioDto

    @GET("portfolio_malformed.json")
    suspend fun getMalformedPortfolio(): PortfolioDto
}