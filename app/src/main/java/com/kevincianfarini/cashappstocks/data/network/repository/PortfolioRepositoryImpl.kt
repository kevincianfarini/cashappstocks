package com.kevincianfarini.cashappstocks.data.network.repository

import com.kevincianfarini.cashappstocks.data.models.Portfolio
import com.kevincianfarini.cashappstocks.data.network.NetworkType
import com.kevincianfarini.cashappstocks.data.network.clients.PortfolioClient

class PortfolioRepositoryImpl(
    private val client: PortfolioClient
) : PortfolioRepository {

    override suspend fun getPortfolio(type: NetworkType) = Portfolio.fromDto(when (type) {
        NetworkType.REGULAR -> client.getCurrentPortfolio()
        NetworkType.EMPTY -> client.getEmptyPortfolio()
        NetworkType.ERROR -> client.getMalformedPortfolio()
    })
}