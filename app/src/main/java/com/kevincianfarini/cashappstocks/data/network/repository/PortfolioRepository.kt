package com.kevincianfarini.cashappstocks.data.network.repository

import com.kevincianfarini.cashappstocks.data.models.Portfolio
import com.kevincianfarini.cashappstocks.data.network.NetworkType

interface PortfolioRepository {

    suspend fun getPortfolio(type: NetworkType): Portfolio
}