package com.kevincianfarini.cashappstocks.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.text.NumberFormat
import java.util.*

/**
 * An amount of money
 */
@Parcelize data class Monetary(
    val currencyUnit: Currency,
    val centsValue: Long
) : Parcelable

private val Monetary.doubleValue get() = centsValue / 100.0

operator fun Monetary.times(amount: Int) = copy(centsValue = centsValue * amount)

/**
 * A display friendly [String] representation of [Monetary]
 */
val Monetary.displayString: String get() = NumberFormat.getCurrencyInstance().apply {
    maximumFractionDigits = 2
    currency = currencyUnit
}.format(doubleValue)