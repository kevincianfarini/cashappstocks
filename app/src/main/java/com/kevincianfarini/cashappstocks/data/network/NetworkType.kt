package com.kevincianfarini.cashappstocks.data.network

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * The different types of requests that can be used to fetch data from
 * the google cloud store API.
 */
@Parcelize enum class NetworkType : Parcelable {
    REGULAR, EMPTY, ERROR
}
