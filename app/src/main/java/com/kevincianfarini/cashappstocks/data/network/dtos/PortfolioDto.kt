package com.kevincianfarini.cashappstocks.data.network.dtos

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PortfolioDto(
    val stocks: List<StockDto> = emptyList()
)