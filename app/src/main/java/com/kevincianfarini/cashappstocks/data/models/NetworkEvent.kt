package com.kevincianfarini.cashappstocks.data.models

sealed class NetworkEvent

object NetworkPending : NetworkEvent()

object NetworkSuccess : NetworkEvent()

class NetworkFailure(val error: Throwable) : NetworkEvent()