package com.kevincianfarini.cashappstocks.data.models

import android.os.Parcelable
import com.kevincianfarini.cashappstocks.data.network.dtos.StockDto
import kotlinx.android.parcel.Parcelize
import java.util.*

/**
 * A single stock within a portfolio
 */
@Parcelize data class Stock(
    val ticker: String,
    val name: String,
    val currentPrice: Monetary,
    val quantity: Int,
    val lastUpdated: Date
) : Parcelable {
    companion object {
        fun fromDto(dto: StockDto) = Stock(
            ticker = dto.ticker,
            name = dto.name,
            currentPrice = Monetary(
                currencyUnit = Currency.getInstance(dto.currency),
                centsValue = dto.currentPriceCents
            ),
            quantity = dto.quantity ?: 0,
            lastUpdated = Date(dto.currentPriceTimeStamp * 1000L)
        )
    }
}