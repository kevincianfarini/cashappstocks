package com.kevincianfarini.cashappstocks.data.models

import android.os.Parcelable
import com.kevincianfarini.cashappstocks.data.network.dtos.PortfolioDto
import kotlinx.android.parcel.Parcelize

/**
 * A grouping of stock data
 */
@Parcelize class Portfolio(val stocks: List<Stock>) : Parcelable {
    companion object {
        fun fromDto(dto: PortfolioDto) = Portfolio(
            stocks = dto.stocks.map(Stock.Companion::fromDto)
        )
    }
}