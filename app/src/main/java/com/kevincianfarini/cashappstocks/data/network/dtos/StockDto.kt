package com.kevincianfarini.cashappstocks.data.network.dtos

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class StockDto(
    val ticker: String,
    val name: String,
    val currency: String,
    @Json(name = "current_price_cents") val currentPriceCents: Long,
    val quantity: Int?,
    @Json(name = "current_price_timestamp") val currentPriceTimeStamp: Long
)