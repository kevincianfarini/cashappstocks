package com.kevincianfarini.cashappstocks.di

import com.kevincianfarini.cashappstocks.data.network.clients.PortfolioClient
import com.kevincianfarini.cashappstocks.data.network.repository.PortfolioRepository
import com.kevincianfarini.cashappstocks.data.network.repository.PortfolioRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(ActivityComponent::class)
object PortfolioModule {

    @Provides fun providesPortfolioClient(): PortfolioClient = Retrofit.Builder()
        .baseUrl("https://storage.googleapis.com/cash-homework/cash-stocks-api/")
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
        .create(PortfolioClient::class.java)

    @Provides fun providesPortfolioRepository(client: PortfolioClient): PortfolioRepository {
        return PortfolioRepositoryImpl(client)
    }

    @Provides
    @Named("viewModelScope")
    fun providesViewModelCoroutineScope(): CoroutineScope = CoroutineScope(
        Dispatchers.Main.immediate + SupervisorJob()
    )
}